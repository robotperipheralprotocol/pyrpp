#!/usr/bin/python2
import time
import serial
import fcntl

HEADER_SEPERATOR = ' '
BODY_SEPERATOR = chr(2)
END_MESSAGE = chr(3)


def parse_message(msg):
    '''parses a message from a string into a RppMessage'''
    if msg.find(BODY_SEPERATOR) == -1:
        raise ClientError(400, msg)
    sep1 =  msg.split(BODY_SEPERATOR)
    header = sep1[0]
    body = BODY_SEPERATOR.join(sep1[1:])
    
    
    if header.find(HEADER_SEPERATOR) == -1:
        raise ClientError(400, msg)

    sep2 = header.split(HEADER_SEPERATOR)
    code = int(sep2[0])
    uri = HEADER_SEPERATOR.join(sep2[1:])
    
    body = body.split(END_MESSAGE)[0]
    out = RppMessage(code, uri, body)
    if code != 200:
        raise ResponseError(out)
    return out

    

class ResponseError(Exception):
    '''Error with response (remote)'''
    def __init__(self, message):
        super(ResponseError, self).__init__(self)
        self.response = message

    def __str__(self):
        return "{}: {} ({} {})".format(self.response.code,
                               RESPONSE_CODES[self.response.code],
                               self.response.uri,
                               self.response.body)

    def get_short_error(self):
        return "{}: {}".format(self.response.code,
                               RESPONSE_CODES[self.response.code])


class ClientError(Exception):
    '''Error on the python client end (locally)'''
    def __init__(self, code, msg=''):
        super(ClientError, self).__init__(self)
        self.response = RppMessage(code, '?', msg)

    def __str__(self):
        return "{}: {}".format(self.response.code,
                               RESPONSE_CODES[self.response.code])

    def get_short_error(self):
        return "{}: {}".format(self.response.code,
                               RESPONSE_CODES[self.response.code])


class RppMessage(object):
    '''A generic RppMessage consisting of a code, uri and body'''
    def __init__(self, code, uri, body):
        self.code = code
        self.uri = uri
        self.body = body

    def __str__(self):
        '''Generates a message string for a request'''
        out_str = '{}{}{}{}{}{}'.format(self.code, HEADER_SEPERATOR,
                                       self.uri, BODY_SEPERATOR, self.body,
                                       END_MESSAGE)
        return out_str


class RppClient(object):
    '''Low level access to a serial rpp client'''
    def __init__(self, port, baud, timeout=1.5):
        self.serial = serial.serial_for_url(port, baud, do_not_open=True)
        self.serial.open()
        try:
            fcntl.flock(self.serial, fcntl.LOCK_EX | fcntl.LOCK_NB)
        except BlockingIOError as e:
            self.close()
            raise e
        #self.serial = serial.Serial(port, baud)
        
        time.sleep(timeout)  # Wait for arduino nano to boot
        self.timeout = timeout
        self.data = ""
        self.name = self.get('/meta/name', timeout=timeout).body
        

    def close(self):
        fcntl.flock(self.serial, fcntl.LOCK_UN | fcntl.LOCK_NB)
        self.serial.close()
        
    

    def get(self, uri, body='', timeout=None):
        '''Preforms a get request and returns the result'''
        self.write(RppMessage('GET', uri, body))
        return self.read(timeout)

    def put(self, uri, body, timeout=None):
        '''Performs a put request and returns the result'''
        self.write(RppMessage('PUT', uri, body))
        return self.read(timeout)

    def write(self, message):
        '''Writes to serial port at low level'''
        self.serial.write(str(message).encode('utf-8'))

    def read(self, timeout=None):
        '''Reads from serial port with timeout'''
        start_time = time.time()
        clean = False
        if timeout == None:
            timeout = self.timeout
        while time.time() < start_time + timeout:
            if self.serial.inWaiting() > 0:
                new = self.serial.read()
                try:
                    self.data += new.decode('utf-8')
                except:
                    self.data += ''
                if self.data.endswith(END_MESSAGE):
                    clean = True
                    break
        if not clean:
            raise ClientError(408, self.data)
        try:
            message = parse_message(self.data)
        except Exception as e:
            self.data = ""
            raise e
        self.data = ""
        return message

RESPONSE_CODES = {
    102: "PROCESSING",
    200: "OK",
    400: "BAD_REQUEST",
    404: "NOT_FOUND",
    405: "NOT_ALLOWED",
    406: "NOT_ACCEPTABLE",
    408: "REQUEST_TIMEOUT",
    412: "PRECONDITION_FAILED",
    413: "PAYLOAD_TOO_LARGE",
    414: "URI_TOO_LONG",
    416: "RANGE_NOT_SATISFIABLE",
    418: "IM_A_TEAPOT",
    423: "LOCKED",
    429: "TOO_MANY_REQUESTS",
    500: "SERVER_ERROR",
    501: "NOT_IMPLEMENTED",
    507: "INSUFFICIENT_STORAGE",
    
    600: "ASYNC_DATA_AVAILABLE",
    610: "ASYNC_INFORMATION",
    611: "ASYNC_DEBUG",
    612: "ASYNC_WARNING",
    613: "ASYNC_ERROR",
    620: "ASYNC_DANGER_TO_TASK",
    630: "ASYNC_DANGER_TO_PERIPHERAL",
    640: "ASYNC_DANGER_TO_EXTERNAL",
    650: "ASYNC_DANGER_TO_HUMAN",
    655: "ASYNC_DANGER_TO_HUMAN_LIFE",
    660: "ASYNC_APOCOLYPSE",
}

RESPONSE_MESSAGES = {RESPONSE_CODES[o]:o for o in RESPONSE_CODES}

if __name__ == "__main__":
    a = RppClient('/dev/ttyUSB0', 115200)
    print(a.name)
